---
MorpheusModelID: M0005

authors: [M. L. Rosenzweig]

title: "Predator-Prey Model by Rosenzweig"

date: "2021-02-22T17:04:00+01:00"
lastmod: "2021-02-23T16:54:00+01:00"

tags:
- Logger
- Lotka–Volterra Equations
- Predator–Prey

menu:
  Built-in Examples:
    parent: ODE
    weight: 50
weight: 60
---
## Introduction

This model implements an extension of the well-known [Lotka-Volterra system](https://en.wikipedia.org/wiki/Lotka%E2%80%93Volterra_equations).

It illustrates how to 

- create a simple ODE model and
- log and plot data as time course.

![](predator-prey.png "Output of predator-prey example model.")

## Description

The [Rosenzweig model](#reference) is the Lotka-Volterra model with logistic growth and type 2 functional response.

First, the `Space` and `Time` of a simulation is specified, here defined as a single lattice site and $5000\ \mathrm{atu}$ (arbitrary time units). In `Global` (see figure below), two `Variable`s for predator and prey densities are set up. The differential equations themselves are specified in a `System` which consists of a number of `Constant`s and two `DiffEqn` (differential equations) and are computed using the `runge-kutta` solver. 

![](predator-prey-global-section.png "Global section of the model.")

Output in terms of a text file as well as a plot is created by a `Logger`, the plugin in the `Analysis` section.

## Things to try

- Bring the system into a stable steady state (fixed point, no oscillations) by changing parameters in `Global`/`System`/`Constant`.

## Reference

>M. L. Rosenzweig: [Paradox of Enrichment: Destabilization of Exploitation Ecosystems in Ecological Time][reference]. *Science* **171** (3969): 385-387, 1971.

[reference]: https://doi.org/10.1126/science.171.3969.385