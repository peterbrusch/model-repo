---
MorpheusModelID: M0031

title: "Coupling Spatial Models: Autocrine Chemotaxis"
date: "2019-11-07T10:42:00+01:00"
lastmod: "2020-10-30T13:29:00+01:00"

aliases: [/examples/coupling-spatial-models-autocrine-chemotaxis/]

menu:
  Built-in Examples:
    parent: Multiscale Models
    weight: 10
weight: 170
---

## Introduction

This example models a cellular Potts model for autocrine chemotaxis by coupling a CPM to a diffusive chemoattractant.

![](cpm-pde.png "Cell cluster due to chemotaxis towards self-produced chemoattractant.")

## Description

It exemplifies the combination of a ```CPM``` with a ```PDE``` through the chemotaxis towards a diffusive chemoattractant produced by cells. The ```CellType``` has a ```Property``` $p$ (production rate) which is positive within biological cells, and is zero in the ```medium``` cell type. The symbol $p$ can than be used in the ```PDE``` as the production term for species or ```Layer``` $U$. In this way, $U$ is produced in each lattice node that is occupied by a cell, and is not produced outside of cells. At the same time, cells perform ```Chemotaxis``` towards the chemoattractant $U$.

The simulation is visualized using some advanced ```Gnuplotter``` feature. Cells are ```superimposed``` on the PDE and appear as transparent domains (see ```opacity```). Moreover, the concentration gradient of the PDE is emphasized by the use of ```isolines``` that draws contours as positions with equal concentration.

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/47176046?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>