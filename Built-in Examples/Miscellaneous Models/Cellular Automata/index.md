---
MorpheusModelID: M0041

title: "Game of Life: Cellular Automata"
date: "2019-11-10T09:58:00+01:00"
lastmod: "2020-10-30T13:46:00+01:00"

aliases: [/examples/game-of-life-cellular-automata/]

menu:
  Built-in Examples:
    parent: Miscellaneous Models
    weight: 10
weight: 510
---
## Introduction

This example models probably the best-known classic cellular automaton (CA) model: [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life). It shows an alternative use of ```System``` for synchronous updating of ```Equations```.

![](game-of-life.png "Conway´s Game of Life.")

## Description

In this model, the lattice is filled with cells of size $1$. Each cell counts the number of neighboring cells that are 'alive' and acts accordingly. The rules that make up the Game of Life are implemented in a ```System``` of ```Equations``` in which all ```Equations``` are updated synchronously.

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/47171614?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Things to try

- Change the ```Neighborhood``` from a Moore (2nd order) to von Neumann (1st order).