---
MorpheusModelID: M0044

title: "FlipCells: Particle Aggregation"
date: "2019-11-10T10:16:00+01:00"
lastmod: "2020-10-30T13:52:00+01:00"

menu:
  Built-in Examples:
    parent: Miscellaneous Models
    weight: 30
weight: 530
---
## Introduction

This models approximates an interacting particle system (IPS) model of particle aggregation. Each black dot represents a particle that moved due to spin flips with random neighbors. The particles perform random walks in which the probability of moving depends on the number of neighboring cells.

![](particle-aggregation.png "Aggregation of moving particles.")

## Description

Each lattice site (white or black) counts the number of particles (black neighboring sites) using a ```NeighborsReporter```. 

The probability of movement of each particle is made dependent on its number of neighbors by using it in the ```Condition``` of ```FlipCells```. When this condition is satisfied, the particle changes positions with a random neighboring lattice site.

A ```PopulationReporter``` is used to return the fraction of isolated black particles. This number is logged and plotted using the ```Logger```.

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/111263973?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Things to try

- Change the parameter $p$.