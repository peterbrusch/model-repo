<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>This example serves to demonstrate the implementation of cells consuming a substrate field e.g. cytokine.

The logic is like this: In the PDE that describes the reaction and diffusion of the substrate, you include a negative term (i.e. for decrease) that is nonzero where there is a cell and zero where there is no cell.

Steps to reproduce: 
1. Define a Field in Global/ e.g. called "cytokine" with an inital concentration 1.0 and diffusion
2. Define a System with a differential equation with a negative term, e.g. "-r*cell*cytokine" where "r" is the consumption rate of cells. (Here it is constant, but could also be a cell property to specify that each cell has its own consumption rate)
3. Define a Constant called "cell" which is "0" everywhere, except for where there is a cell. 
    To do this specify "cell" in Global and set it to "0".
    But in CellType , the value of the constant is overwritten with a value of "1".

In this way, the term "-r*cell*cytokine" is negative 'within' cells and zero outside. Therefore, the cytokine concentration is only decreased at locations with cells.

Date: March 4, 2022 
	Author: walter@deback.net 
	Contributor: ferras_mahmoud.el-hendi@tu-dresden.de 
	Software: Morpheus (open-source), download from https://morpheus.gitlab.io 
</Details>
        <Title>Cells' Cytokine Consumption</Title>
    </Description>
    <Space>
        <Lattice class="square">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <Size symbol="size" value="200, 200, 0"/>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="100"/>
        <TimeSymbol symbol="time"/>
    </Time>
    <CellTypes>
        <CellType class="biological" name="cells">
            <Constant symbol="cell" value="1"/>
        </CellType>
    </CellTypes>
    <CellPopulations>
        <Population type="cells" size="1">
            <InitCellObjects mode="distance">
                <Arrangement displacements="0,0,0" repetitions="1,1,0">
                    <Sphere radius="25.0" center="size/2"/>
                </Arrangement>
            </InitCellObjects>
        </Population>
    </CellPopulations>
    <Global>
        <Constant symbol="cell" value="0.0" name="Presence of cell"/>
        <Field symbol="cytokine" value="1.0" name="Cytokine concentration">
            <Diffusion rate="1"/>
        </Field>
        <System solver="Dormand-Prince [adaptive, O(5)]">
            <Constant symbol="r" value="0.05" name="consumption rate"/>
            <DiffEqn symbol-ref="cytokine">
                <Expression>-r*cell*cytokine</Expression>
            </DiffEqn>
        </System>
    </Global>
    <Analysis>
        <Gnuplotter time-step="1">
            <Plot>
                <Cells min="0" value="cell" max="1"/>
            </Plot>
            <Plot>
                <Field symbol-ref="cytokine" min="0" max="1">
                    <ColorMap>
                        <Color color="red" value="1"/>
                        <Color color="white" value="0"/>
                    </ColorMap>
                </Field>
            </Plot>
            <Terminal name="png"/>
        </Gnuplotter>
        <ModelGraph format="dot" include-tags="#untagged" reduced="false"/>
    </Analysis>
</MorpheusModel>
