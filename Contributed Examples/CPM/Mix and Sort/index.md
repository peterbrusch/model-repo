---
MorpheusModelID: M5480

title: Repeated Adhesion-based Mixing and Sorting of Cells

contributors: [W. de Back, F. El-Hendi]

categories:
- DOI:10.1103/PhysRevLett.69.2013

tags:
- 2D
- Adhesion
- Contributed Examples
- Cell-Cell Adhesion
- Cellular Potts Model
- CPM
- Homophilic Adhesion
- HomophilicAdhesion
- Interaction
---

>Changing the adhesion parameters `CPM`/`Interaction`/`Contact` `value` during simulation runtime

## Introduction

This example model demonstrates the use of variable adhesion values in the `CPM`/`Interaction`/`Contact`/`HomophilicAdhesion` plugin. Please also see the built-in docu at `MorpheuML`/`CPM`/`Interaction`/`Contact`/`HomophilicAdhesion`. The same is possible for the `HeterophilicAdhesion` as well. Here the adhesive variable is time-dependent, but other forms are possible as well, e.g. space-dependent. This example was motivated by a Morpheus user in the [Morpheus user forum](https://groups.google.com/g/morpheus-users/c/M6F0S8VxyBo)

## Description

Based on the [built-in example](/model/built-in-examples/) `Examples` → `CPM` → `CellSorting_2D.xml` ([M0021](/model/M0021/)), a cell Property `c` (for cadherin) is created and this is used as a symbol for the adhesive in `HomophilicAdhesion` for the contact between the celltypes `ct1` and `ct2`. The value of `c` is changed by the `Equation`:

```
c = sin(time / 1000.0) + 1 
```

which modulates cell-cell adhesion as a function of time according to a sine wave. 

As a result, the cell types `ct1` and `ct2` will sort out, then mix, sort out again, mix etc.

Movie visualising the change in adhesion properties over time:

![Movie visualising the change in adhesion properties over time.](mix_and_sort.mp4)

The panel shows cell type `ct1` (red) and `ct2` (yellow).
