<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>Two-Parameter plane showing all possible regimes of behaviour for  a single CPM cell.
Analysis in L Edeltein-Keshet (2021) Mathematical Models in Cell BIology

Idea is based on Fig 3 in the followng paper:

Magno, R., Grieneisen, V. A., &amp; Marée, A. F. (2015). The biophysical nature of cells: potential cell behaviours revealed by analytical and computational studies of cell surface mechanics. BMC biophysics, 8(1), 1-37.

But here, we use two dimensionless parameters,

dimensionless perimeter cost:   kappa=4*LambP/(LambA*Ra^2)  - on the horizontal axis

dimensionless cell-medium contact energy cost:  beta= 2*Jmed/(3.14*LambA*Ra^3)  - on the vertical axis

circularity: alpha = R_p/R_a - vary manually 

Suggestion: try the settings alpha=0.5, 1, 2.

The results are now consistent with theoretical analysis in the above references.
However, fine-tuning of the CPM settings was required to obtain the results.
======================================

Simulation was fixed by Joern Staruss (JS), and some comments added based on his help.

JS: Morpheus 2.2 supports expressions in Contact energies directly, which may depend on properties of the given cell types. For example,
&lt;Contact type1="Cell" type2="medium" value="cell1.Jmed">
&lt;/Contact>

In Previous versions of Morpheus, this had to be done by AddOn adhesion.

JS: There is a constraint that prevents cells from vanishing from volume 1 to 0. You may use a CellDeath component to remove cells of volume 1. Usually we don't want cells to disappear due to shape constraint imbalances.


JS: Creating a cell array with an initial shape works nicely with InitCellObjects 
&lt;InitCellObjects mode="distance">
 &lt;Annotation>Initialize an 8x8 cell array  in a  square domain.&lt;/Annotation>
 &lt;Arrangement displacements="96, 96, 0" repetitions="8, 8, 1">
  &lt;Sphere radius="20.0" center="40.0, 40.0, 0.0"/>
 &lt;/Arrangement>
&lt;/InitCellObjects>


JS: CPM dynamics have a lot of technical details in the CPM section. The simulation can get locked into local energy minima. JS increased the amplitude of fluctuations and smoothed the shape surface length approximation by choosing  ShapeSurface/Neighborhood order 6 for the square lattice. Also choosing updates from Neighborhood order 2 is probably sufficient. This helped to get the theoretical results.
--> MorpheusModel/CPM/MonteCarloSampler/MetropolisKinetics/temperature = 40
--> MorpheusModel/CPM/ShapeSurface/Neighborhood/Order  =  6 
--> MorpheusModel/CPM/MonteCarloSampler/Neighborhood/Order  = 2


</Details>
        <Title>CPMParameterPlane</Title>
    </Description>
    <Space>
        <Lattice class="square">
            <Annotation>Set up the square grid</Annotation>
            <Neighborhood>
                <Order>4</Order>
            </Neighborhood>
            <Size value="750,750,0" symbol="size"/>
            <BoundaryConditions>
                <Condition boundary="x" type="noflux"/>
                <Condition boundary="y" type="noflux"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="2000"/>
        <TimeSymbol symbol="time"/>
    </Time>
    <CellTypes>
        <CellType name="Cell" class="biological">
            <VolumeConstraint target="pi*(Ra^2)" strength="LambA"/>
            <SurfaceConstraint target="alpha*CircPerim" strength="LambP" mode="surface">
                <Annotation>JS: changed the surface constraint to an absolute value, not the relative-to-current-volume aspherity. LEK: alpha is still the same dimensionless ratio of radii</Annotation>
            </SurfaceConstraint>
            <Annotation>Initialize the cell with a volume and a perimeter constraint. The simulation will vary Jmed and LambP to get the distinct bergimes of behaviour. </Annotation>
            <Property name="Radius of rest area" value="20" symbol="Ra">
                <Annotation>Pick a convenient rest area radius so the cells will be visible.</Annotation>
            </Property>
            <Property value="cell.center.x/80" symbol="LambP"/>
            <Property value="10/(Ra^2)" symbol="LambA">
                <Annotation>To keep the "volume constraint" reasonable for larger radii, this parameter is scaled so that LambA*Ra^2=10 is fixed</Annotation>
            </Property>
            <Property name="cell-medium contact energy term" value="6*(cell.center.y-250)" symbol="Jmed"/>
            <Property name="Ratio of Rp to Ra" value="1" symbol="alpha">
                <Annotation>This is a dimensionless parameter that can be varied between 0 and something > 1. It is the ratio of the perimeter to the area rest radii</Annotation>
            </Property>
            <Property value="2*pi*Ra" symbol="CircPerim"/>
            <Property value="4*LambP/(LambA*Ra^2)" symbol="kappa">
                <Annotation>Dimensionless parameter varied for cells along the horizontal axis</Annotation>
            </Property>
            <Property value="2*Jmed/(pi*LambA*Ra^3)" symbol="beta">
                <Annotation>Dimensionless parameter varied for cells along the y axis</Annotation>
            </Property>
        </CellType>
        <CellType name="medium" class="medium"/>
    </CellTypes>
    <CPM>
        <Interaction>
            <Contact type1="Cell" value="200" type2="Cell"/>
            <Contact type1="Cell" value="cell1.Jmed" type2="medium"/>
        </Interaction>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>6</Order>
            </Neighborhood>
        </ShapeSurface>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <MetropolisKinetics temperature="20"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </MonteCarloSampler>
    </CPM>
    <CellPopulations>
        <Population size="1" type="Cell">
            <InitCellObjects mode="distance">
                <Annotation>Initialize the cells in a regular square domain, so that the x and y coordinates of each cell will correspond to its CPm behaviour.</Annotation>
                <Arrangement repetitions="8, 8, 1" displacements="96, 96, 0">
                    <Sphere center="40.0, 40.0, 0.0" radius="20.0"/>
                </Arrangement>
            </InitCellObjects>
        </Population>
    </CellPopulations>
    <Analysis>
        <Gnuplotter decorate="true" time-step="100">
            <Plot>
                <Cells flooding="true" value="beta"/>
            </Plot>
            <Terminal name="png"/>
            <Plot>
                <Cells flooding="true" value="kappa"/>
            </Plot>
            <Annotation>The two plots are the same, but one is  colored by the beta value and the other by the kappa value. This also provides a scale for each.</Annotation>
        </Gnuplotter>
        <ModelGraph include-tags="#untagged" reduced="false" format="svg"/>
    </Analysis>
    <Global>
        <Constant name="Number of cells to display" value="64" symbol="NumCells"/>
        <Variable value="0.0" symbol="kappa"/>
        <Variable value="0" symbol="Jmed"/>
        <Variable value="0.0" symbol="beta"/>
    </Global>
</MorpheusModel>
